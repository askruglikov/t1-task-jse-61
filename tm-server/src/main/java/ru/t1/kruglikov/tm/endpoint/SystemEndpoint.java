package ru.t1.kruglikov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.kruglikov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.kruglikov.tm.api.service.ILocatorService;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.kruglikov.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.kruglikov.tm.dto.request.system.SystemInfoRequest;
import ru.t1.kruglikov.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.kruglikov.tm.dto.response.system.ApplicationVersionResponse;
import ru.t1.kruglikov.tm.dto.response.system.SystemInfoResponse;
import ru.t1.kruglikov.tm.util.FormatUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.kruglikov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @Autowired
    public SystemEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = getLocatorService().getPropertyService();

        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = getLocatorService().getPropertyService();

        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public SystemInfoResponse getApplicationInfo(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SystemInfoRequest request
    ) {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        @NotNull final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        @NotNull final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        @NotNull final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;

        @NotNull final SystemInfoResponse response = new SystemInfoResponse();
        response.setAvailableProcessors(availableProcessors);
        response.setFreeMemoryFormat(freeMemoryFormat);
        response.setMaxMemoryValue(maxMemoryValue);
        response.setTotalMemoryFormat(totalMemoryFormat);
        response.setUsageMemoryFormat(usageMemoryFormat);
        return response;
    }

}