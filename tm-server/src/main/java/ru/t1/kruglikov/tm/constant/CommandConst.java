package ru.t1.kruglikov.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class CommandConst {

    @NotNull public static final String CMD_HELP = "help";
    @NotNull public static final String CMD_VERSION = "version";
    @NotNull public static final String CMD_ABOUT = "about";
    @NotNull public static final String CMD_INFO = "info";
    @NotNull public static final String CMD_ARGUMENT = "arguments";
    @NotNull public static final String CMD_COMMAND = "commands";

    @NotNull public static final String CMD_PROJECT_CREATE = "project-create";
    @NotNull public static final String CMD_PROJECT_LIST = "project-list";
    @NotNull public static final String CMD_PROJECT_SHOW_BY_ID = "project-show-by-id";
    @NotNull public static final String CMD_PROJECT_SHOW_BY_INDEX = "project-show-by-index";
    @NotNull public static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";
    @NotNull public static final String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    @NotNull public static final String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    @NotNull public static final String CMD_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    @NotNull public static final String CMD_PROJECT_CLEAR = "project-clear";
    @NotNull public static final String CMD_PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";
    @NotNull public static final String CMD_PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";
    @NotNull public static final String CMD_PROJECT_START_BY_ID = "project-start-by-id";
    @NotNull public static final String CMD_PROJECT_START_BY_INDEX = "project-start-by-index";
    @NotNull public static final String CMD_PROJECT_COMPLETE_BY_ID = "project-complete-by-id";
    @NotNull public static final String CMD_PROJECT_COMPLETE_BY_INDEX = "project-complete-by-index";

    @NotNull public static final String CMD_TASK_CREATE = "task-create";
    @NotNull public static final String CMD_TASK_LIST = "task-list";
    @NotNull public static final String CMD_TASK_SHOW_BY_ID = "task-show-by-id";
    @NotNull public static final String CMD_TASK_SHOW_BY_INDEX = "task-show-by-index";
    @NotNull public static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";
    @NotNull public static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";
    @NotNull public static final String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";
    @NotNull public static final String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    @NotNull public static final String CMD_TASK_CLEAR = "task-clear";
    @NotNull public static final String CMD_TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";
    @NotNull public static final String CMD_TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";
    @NotNull public static final String CMD_TASK_START_BY_ID = "task-start-by-id";
    @NotNull public static final String CMD_TASK_START_BY_INDEX = "task-start-by-index";
    @NotNull public static final String CMD_TASK_COMPLETE_BY_ID = "task-complete-by-id";
    @NotNull public static final String CMD_TASK_COMPLETE_BY_INDEX = "task-complete-by-index";

    @NotNull public static final String CMD_TASK_BIND_TO_PROJECT = "task-bind-to-project";
    @NotNull public static final String CMD_TASK_UNBIND_FROM_PROJECT = "task-unbind-from-project";
    @NotNull public static final String CMD_TASK_SHOW_BY_PROJECT_ID = "task-show-by-project-id";

    @NotNull public static final String CMD_EXIT = "exit";

}
