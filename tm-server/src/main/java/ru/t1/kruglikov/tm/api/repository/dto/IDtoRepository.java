package ru.t1.kruglikov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.kruglikov.tm.dto.model.AbstractModelDTO;

@Repository
@Scope("prototype")
public interface IDtoRepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

}


