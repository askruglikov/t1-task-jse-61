package ru.t1.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.kruglikov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.dto.request.user.UserLoginRequest;
import ru.t1.kruglikov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.kruglikov.tm.service.PropertyService;
import ru.t1.kruglikov.tm.marker.IntegrationCategory;

import static ru.t1.kruglikov.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testLogin() {
        @NotNull final UserLoginRequest request = new UserLoginRequest(null);
        request.setLogin(USER_LOGIN);
        request.setPassword(USER_PASSWORD);
        Assert.assertNotNull(authEndpoint.login(request).getToken());

        @NotNull final UserLoginRequest requestException = new UserLoginRequest(null);
        thrown.expect(Exception.class);
        authEndpoint.login(requestException);
    }

    @Test
    public void testLogout() {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest(null);
        requestLogin.setLogin(USER_LOGIN);
        requestLogin.setPassword(USER_PASSWORD);
        @NotNull final String token = authEndpoint.login(requestLogin).getToken();
        Assert.assertNotNull(token);

        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(token);
        Assert.assertNotNull(authEndpoint.logout(requestLogout));

        @NotNull final UserLogoutRequest requestException = new UserLogoutRequest();
        thrown.expect(Exception.class);
        authEndpoint.logout(requestException);
    }

}
