package ru.t1.kruglikov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static String ADMIN_LOGIN = "admin";

    @NotNull
    public final static String ADMIN_PASSWORD = "admin";

    @NotNull
    public final static String USER_LOGIN = "USER_01";

    @NotNull
    public final static String USER_PASSWORD = "user01";

    @NotNull
    public final static String USER2_LOGIN = "USER_02";

    @NotNull
    public final static String USER2_PASSWORD = "user02";

}
